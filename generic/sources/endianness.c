/**
*** \file endianness.c
*** \brief endianness.h implementation.
*** \details Implementation file for endianness.h.
**/

/*
** This file is part of awisetoolbox.
** Copyright (C) 2008-2018 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/** Identifier for endianness.c. **/
#define   ENDIANNESS_C


/****
*****
***** INCLUDES
*****
****/

#include  "endianness.h"
#if       defined(DEBUG_ENDIANNESS_C)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_ENDIANNESS_C) */
#include  "debuglog.h"
#include  "messagelog.h"


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

BOOLEAN_T IsBigEndian(void)
{
  short TestValue=0xFF;


  DEBUGLOG_Printf0("IsBigEndian()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(TestValue==*((unsigned char *)&TestValue));
}

BOOLEAN_T IsLittleEndian(void)
{
  DEBUGLOG_Printf0("IsLittleEndian()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(!IsBigEndian());
}

uint16_t Swap16(uint16_t Value)
{
  DEBUGLOG_Printf1("Swap16(0x%04X)",Value);
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return((uint16_t)((Value>>8)|(Value<<8)));
}

uint32_t Swap32(uint32_t Value)
{
  DEBUGLOG_Printf1("Swap32(0x%08X)",Value);
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(
      Swap16((uint16_t)(Value>>16))|(((uint32_t)Swap16((uint16_t)Value))<<16));
}

uint64_t Swap64(uint64_t Value)
{
  DEBUGLOG_Printf1("Swap64(0x%016X)",Value);
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(
      Swap32((uint32_t)(Value>>32))|(((uint64_t)Swap32((uint32_t)Value))<<32));
}


#undef    ENDIANNESS_C


/**
*** endianness.c
**/
