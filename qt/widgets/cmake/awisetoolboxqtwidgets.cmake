#
# awisetoolboxqtwidgets.cmake
# CMake makefile include for the A.Wise Toolbox Qt Widget library.
#

#
# This file is part of awisetoolbox.
# Copyright (C) 2008-2018 by Alan Wise <awisesoftware@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


#
# Configuration
#

# Need Qt4.
IF(NOT QT4_FOUND)
  FIND_PACKAGE(Qt4 REQUIRED)
  INCLUDE(${QT_USE_FILE})
ENDIF()


#
# Include paths
#

SET(INCLUDE_DIRECTORIES
    "${PROJECT_SOURCE_DIR}/qt/widgets/sources/"
    "${PROJECT_SOURCE_DIR}/generic/sources/")
STRING(REPLACE ";" "\" \\\n\""
    TEMP_DOXYGEN_INCLUDE_PATHS "\"${INCLUDE_DIRECTORIES}\"")
STRING(CONCAT DOXYGEN_INCLUDE_PATHS
    "${DOXYGEN_INCLUDE_PATHS} \\\n${TEMP_DOXYGEN_INCLUDE_PATHS}")
INCLUDE_DIRECTORIES(
    "${CMAKE_CURRENT_BINARY_DIR}"
    "${INCLUDE_DIRECTORIES}")


#
# Sources
#

SET(QTWIDGETS_SOURCES
    "${PROJECT_SOURCE_DIR}/qt/widgets/sources/aboutwidget.cpp"
    "${PROJECT_SOURCE_DIR}/qt/widgets/sources/debugtools.cpp"
    "${PROJECT_SOURCE_DIR}/qt/widgets/sources/debuglogviewerwidget.cpp"
    "${PROJECT_SOURCE_DIR}/qt/widgets/sources/messagelogviewerwidget.cpp")
SET(QTWIDGETS_HEADERS
    "${PROJECT_SOURCE_DIR}/qt/widgets/sources/aboutwidget.h"
    "${PROJECT_SOURCE_DIR}/qt/widgets/sources/debugtools.h"
    "${PROJECT_SOURCE_DIR}/qt/widgets/sources/debugtools_private.h"
    "${PROJECT_SOURCE_DIR}/qt/widgets/sources/debuglogviewerwidget.h"
    "${PROJECT_SOURCE_DIR}/qt/widgets/sources/messagelogviewerwidget.h")
SET(QTWIDGETS_FORMS
    "${PROJECT_SOURCE_DIR}/qt/widgets/forms/aboutwidget.ui"
    "${PROJECT_SOURCE_DIR}/qt/widgets/forms/debugconsoledialog.ui"
    "${PROJECT_SOURCE_DIR}/qt/widgets/forms/debugconsoleoptionsdialog.ui"
    "${PROJECT_SOURCE_DIR}/qt/widgets/forms/debuglogviewerwidget.ui"
    "${PROJECT_SOURCE_DIR}/qt/widgets/forms/messagelogviewerwidget.ui")
SET(QTWIDGETS_RESOURCES
    "${PROJECT_SOURCE_DIR}/qt/widgets/resources/aboutwidget.qrc"
    "${PROJECT_SOURCE_DIR}/qt/widgets/resources/debugconsole.qrc")
QT4_WRAP_CPP(QTWIDGETS_MOC ${QTWIDGETS_HEADERS})
QT4_WRAP_UI(QTWIDGETS_UI ${QTWIDGETS_FORMS})
QT4_ADD_RESOURCES(QTWIDGETS_QRC ${QTWIDGETS_RESOURCES})
SET(QTWIDGETS_FILES
    ${QTWIDGETS_SOURCES}
    ${QTWIDGETS_HEADERS}
    ${QTWIDGETS_MOC}
    ${QTWIDGETS_UI}
    ${QTWIDGETS_QRC})


#
# Binaries
#


#
# Subdirectories
#


#
# Installation
#


#
# awisetoolboxqtwidgets.cmake
#
