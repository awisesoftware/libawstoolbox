#
# library.cmake
# CMake makefile include for the A.Wise Toolbox Qt Designer plugin.
#

#
# This file is part of awisetoolbox.
# Copyright (C) 2008-2018 by Alan Wise <awisesoftware@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


# Options.
INCLUDE(CMakeDependentOption)
CMAKE_DEPENDENT_OPTION(OPTION_AWISETOOLBOX_QTDESIGNER_BUILDLIBRARY
    "Build the ${DISPLAYNAME}." OFF G_RELEASEFLAG OFF)

IF(OPTION_AWISETOOLBOX_QTDESIGNER_BUILDLIBRARY AND G_RELEASEFLAG)

  #
  # Configuration
  #

  # Need Qt4.
  IF(NOT QT4_FOUND)
    FIND_PACKAGE(Qt4 REQUIRED)
    INCLUDE(${QT_USE_FILE})
  ENDIF()

  ADD_DEFINITIONS(${G_DEFINEPREFIX}BUILD_PLUGIN)


  #
  # Include paths
  #

  INCLUDE_DIRECTORIES("${QT_QTDESIGNER_INCLUDE_DIR}")


  #
  # Sources
  #

  INCLUDE("${PROJECT_SOURCE_DIR}/qt/widgets/cmake/awisetoolboxqtwidgets.cmake")
  SET(SOURCES
      "${SOURCES_DIR}/aboutwidgetplugin.cpp"
      "${SOURCES_DIR}/debuglogviewerwidgetplugin.cpp"
      "${SOURCES_DIR}/messagelogviewerwidgetplugin.cpp"
      "${SOURCES_DIR}/awisetoolboxwidgetsinterface.cpp")
  SET(HEADERS
      "${SOURCES_DIR}/aboutwidgetplugin.h"
      "${SOURCES_DIR}/debuglogviewerwidgetplugin.h"
      "${SOURCES_DIR}/messagelogviewerwidgetplugin.h"
      "${SOURCES_DIR}/awisetoolboxwidgetsinterface.h")
  SET(FORMS )
  SET(RESOURCES )
  QT4_WRAP_CPP(MOC ${HEADERS})
  QT4_WRAP_UI(UI ${FORMS})
  QT4_ADD_RESOURCES(QRC ${RESOURCES})
  SET(FILES
      ${SOURCES}
      ${HEADERS}
      ${MOC}
      ${UI}
      ${QRC})


  #
  # Binaries
  #

  # Build the library.
  ADD_LIBRARY(
      ${LIBRARYNAME} SHARED
      ${FILES}
      ${QTWIDGETS_FILES})
  # Anything undefined causes an error.
  TARGET_LINK_LIBRARIES(${LIBRARYNAME} "-Wl,-z,defs" QtCore QtGui)


  #
  # Subdirectories
  #


  #
  # Installation
  #

  INSTALL(FILES "${CMAKE_CURRENT_BINARY_DIR}/lib${LIBRARYNAME}.so"
      DESTINATION "${QT_PLUGINS_DIR}/designer/")


ENDIF()


#
# library.cmake
#
