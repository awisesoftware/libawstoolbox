/*
** This file is part of awisetoolbox.
** Copyright (C) 2008-2018 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \file awisetoolboxwidgetsinterface.h
*** \brief Qt Designer custom widgets list interface.
*** \details An interface for Qt Designer to obtain a list of custom widgets.
**/


#ifndef   AWISETOOLBOXWIDGETSINTERFACE_H
#define   AWISETOOLBOXWIDGETSINTERFACE_H


/****
*****
***** INCLUDES
*****
****/

#include  <QtDesigner>


/****
*****
***** DEFINES
*****
****/

/**
*** \brief Custom widgets group name.
*** \details The custom widgets are placed under this group name.
**/
#define   AWISETOOLBOXWIDGETSINTERFACE_DESIGNER_GROUPNAME  "A.Wise Toolbox Widgets"


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief Qt Designer custom widgets list interface.
*** \details An interface for Qt Designer to obtain a list of custom widgets.
**/
class AWISETOOLBOXWIDGETSINTERFACE_C :
    public QObject, public QDesignerCustomWidgetCollectionInterface
{
  Q_OBJECT
  Q_INTERFACES(QDesignerCustomWidgetCollectionInterface)

  public:
    /**
    *** \brief Constructor.
    *** \details Constructor.
    *** \param pParent Pointer to parent object.
    **/
    AWISETOOLBOXWIDGETSINTERFACE_C(QObject *pParent=NULL);
    /**
    *** \brief Returns the collection's custom widgets interface list.
    *** \details Returns a list of the collection's custom widgets interfaces.
    *** \returns The list of custom widget interface pointers.
    **/
    QList<QDesignerCustomWidgetInterface*> customWidgets(void) const;

  private:
    /**
    *** \brief Custom widget interface pointer list.
    *** \details The list of custom widget interface pointers.
    **/
    QList<QDesignerCustomWidgetInterface*> m_Widgets;
};


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


#endif    /* AWISETOOLBOXWIDGETSINTERFACE_H */
