/*
** This file is part of awisetoolbox.
** Copyright (C) 2008-2018 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
*** \file updatenotifier.h
*** \brief Checks the internet for a program update.
*** \details Periodically queries a file stored on the internet for a program
***   update.
**/


#ifndef   UPDATENOTIFIER_H
#define   UPDATENOTIFIER_H


/****
*****
***** INCLUDES
*****
****/

#include  "errorcode.h"

#include  <QObject>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

class QNetworkAccessManager;
class QTimer;
class QNetworkReply;

/**
*** \brief Program update checker.
*** \details Checks for a program update. It does so by reading a file from a
***   URL and notifies the program of the version number contained in that file.
**/
class UPDATENOTIFIER_C : public QObject
{
  Q_OBJECT

  public:
    /**
    *** \brief Constructor.
    *** \details Constructor.
    **/
    UPDATENOTIFIER_C(void);
    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~UPDATENOTIFIER_C(void);

    /**
    *** \brief Checks for an update.
    *** \details Immediately checks for an update.
    **/
    void CheckForUpdate(void);
    /**
    *** \brief Sets the update file URL.
    *** \details Sets the URL of the file containing the most recent version
    ***   number.
    *** \param URL File URL.
    **/
    void SetURL(QString URL);
    /**
    *** \brief Sets the update check frequency.
    *** \details Sets how often (in days) to check for an update.
    *** \param Interval Check frequency (in days).
    *** \retval >0 Success.
    *** \retval <0 Failure.
    **/
    ERRORCODE_T SetCheckForUpdateInterval(int Interval);

  private slots:
    /**
    *** \brief Most recent version file downloaded.
    *** \details Function called when the file containing the most recent
    ***   version of the application has been downloaded.
    *** \param pReply Network response to download request.
    **/
    void DownloadCompleteSlot(QNetworkReply *pReply);
    /**
    *** \brief Check for update timer triggered.
    *** \details The timer used to check for an update has triggered.
    **/
    void TimerTriggeredSlot(void);

  signals:
    /**
    *** \brief Most recent version signal.
    *** \details Signal emitted when the file containing the most recent
    ***   version has been downloaded.
    *** \param Version Most recent version.
    **/
    void VersionSignal(QString Version);

  private:
    /**
    *** \brief Update URL.
    *** \details URL used to check for a file containing the most recent
    ***   version.
    **/
    QString m_URL;
    /**
    *** \brief Network access manager.
    *** \details Allows the application to send network request and receive
    ***   replies.
    **/
    QNetworkAccessManager *m_pNetworkAccess;
    /**
    *** \brief Update check timer.
    *** \details Timer used to trigger update checks.
    **/
    QTimer *m_pTimer;
    /**
    *** \brief Checking flag.
    *** \details The widget is in the process of checking for an update.
    **/
    bool m_CheckingFlag;
    /**
    *** \brief Update check frequency.
    *** \details How often (in days) to check for an update.
    **/
    int m_UpdateInterval;
    /**
    *** \brief Next update check.
    *** \details Count (in days) until next check for an update.
    **/
    int m_UpdateIntervalCounter;
};


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


#endif    /* UPDATENOTIFIER_H */
